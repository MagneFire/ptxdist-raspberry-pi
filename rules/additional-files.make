# -*-makefile-*-
#
# Copyright (C) 2016 by <>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_ADDITIONAL_FILES) += additional-files

ADDITIONAL_FILES_VERSION	:= 1.0

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

#$(STATEDIR)/additional-files.get:
#	@$(call targetinfo)
#	@$(call touch)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

#$(STATEDIR)/additional-files.extract:
#	@$(call targetinfo)
#	@$(call touch)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

#$(STATEDIR)/additional-files.prepare:
#	@$(call targetinfo)
#	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

#$(STATEDIR)/additional-files.compile:
#	@$(call targetinfo)
#	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/additional-files.install:
	@$(call targetinfo)

#
# TODO:
# If some of the files are required in root filesystem's build process, install
# these files in the install stage. But use proper variables PTXdist supports
# to install files, instead of fixed paths. The following variables can be
# used:
#
# - $(PTXDIST_SYSROOT_TARGET) points to a directory tree
#   all target relevant libraries and header files are installed to. All
#   packages built for the target are searching in this directory tree for
#   header and library files. These files are for compile time only, not for
#   runtime!
#   Paths:
#    - executables: $(PTXDIST_SYSROOT_TARGET)/bin
#    - header files: $(PTXDIST_SYSROOT_TARGET)/include
#    - libraries: $(PTXDIST_SYSROOT_TARGET)/lib
#
# - $(PTXDIST_SYSROOT_HOST) points to a directory tree all host relevant
#   executables, libraries and header files are installed to. All packages
#   built for the host are searching in this directory tree for executables,
#   header and library files.
#   Paths:
#    - executables: $(PTXDIST_SYSROOT_HOST)/bin
#    - header files: $(PTXDIST_SYSROOT_HOST)/include
#    - libraries: $(PTXDIST_SYSROOT_HOST)/lib
#
# - $(PTXDIST_SYSROOT_CROSS) points to a directory tree all cross relevant
#   executables, libraries and header files are installed to. All packages
#   built for the host to create data for the target are searching in this
#   directory tree for executables, header and library files.
#   Paths:
#    - executables: $(PTXDIST_SYSROOT_CROSS)/bin
#    - header files: $(PTXDIST_SYSROOT_CROSS)/include
#    - libraries: $(PTXDIST_SYSROOT_CROSS)/lib
#
#
# If no compile time files are reqired, skip this stage
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/additional-files.targetinstall:
	@$(call targetinfo)
#
# TODO: To build your own package, if this step requires one
	@$(call install_init, additional-files)
	@$(call install_fixup,additional-files,PRIORITY,optional)
	@$(call install_fixup,additional-files,SECTION,base)
	@$(call install_fixup,additional-files,AUTHOR,"<>")
	@$(call install_fixup,additional-files,DESCRIPTION,missing)
#
# TODO: Add here all files that should be copied to the target
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/lib/firmware/brcm/brcmfmac43430-sdio.bin, /lib/firmware/brcm/brcmfmac43430-sdio.bin)
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/lib/firmware/brcm/brcmfmac43430-sdio.txt, /lib/firmware/brcm/brcmfmac43430-sdio.txt)
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/lib/firmware/brcm/BCM43430A1.hcd, /lib/firmware/brcm/BCM43430A1.hcd)
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/etc/firmware/BCM43430A1.hcd, /etc/firmware/BCM43430A1.hcd)
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/usr/bin/init-bluetooth, /usr/bin/init-bluetooth)
	# Install the startup script.
	@$(call install_copy, additional-files, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/etc/systemd/system/init-bluetooth.service, /etc/systemd/system/init-bluetooth.service)
	@$(call install_link, additional-files, ../init-bluetooth.service, /etc/systemd/system/multi-user.target.wants/init-bluetooth.service)

# Note: Add everything before(!) call to macro install_finish
#
#	@$(call install_copy, additional-files, 0, 0, 0755, $(ADDITIONAL_FILES_DIR)/foobar, /dev/null)
#
	@$(call install_finish,additional-files)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

#$(STATEDIR)/additional-files.clean:
#	@$(call targetinfo)
#	@$(call clean_pkg, ADDITIONAL_FILES)

# vim: syntax=make
