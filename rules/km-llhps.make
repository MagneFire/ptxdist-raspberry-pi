# -*-makefile-*-
#
# Copyright (C) 2016 by <>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_KM_LLHPS) += km-llhps

#
# Paths and names and versions
#
KM_LLHPS_VERSION	:= 1.0
KM_LLHPS		:= km-llhps-$(KM_LLHPS_VERSION)
KM_LLHPS_URL		:= lndir://$(PTXDIST_WORKSPACE)/local_src/km-llhps
KM_LLHPS_DIR		:= $(BUILDDIR)/$(KM_LLHPS)
KM_LLHPS_LICENSE	:= unknown

ifdef PTXCONF_KM_LLHPS
$(STATEDIR)/kernel.targetinstall.post: $(STATEDIR)/km-llhps.targetinstall
endif

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

$(STATEDIR)/km-llhps.prepare:
	@$(call targetinfo)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/km-llhps.compile:
	@$(call targetinfo)
	$(KERNEL_PATH) $(KERNEL_ENV) $(MAKE) $(KERNEL_MAKEVARS) \
		-C $(KERNEL_DIR) \
		M=$(KM_LLHPS_DIR) \
		modules
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/km-llhps.install:
	@$(call targetinfo)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/km-llhps.targetinstall:
	@$(call targetinfo)
	@$(call install_init, km-llhps)
	@$(call install_fixup, km-llhps, PRIORITY, optional)
	@$(call install_fixup, km-llhps, SECTION, base)
	@$(call install_fixup, km-llhps, AUTHOR, "Taco Eelman<>")
	@$(call install_fixup, km-llhps, DESCRIPTION, missing)

	$(KERNEL_PATH) $(KERNEL_ENV) $(MAKE) $(KERNEL_MAKEVARS) \
		-C $(KERNEL_DIR) \
		M=$(KM_LLHPS_DIR) \
		modules_install
	# Install the startup script.
	@$(call install_copy, km-llhps, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/usr/bin/init-lcd, /usr/bin/init-lcd)
	@$(call install_copy, km-llhps, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/etc/systemd/system/km-llhps.service, /etc/systemd/system/km-llhps.service)
	@$(call install_link, km-llhps, ../km-llhps.service, /etc/systemd/system/network.target.wants/km-llhps.service)
	@$(call install_finish, km-llhps)
	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

#$(STATEDIR)/km-llhps.clean:
#	@$(call targetinfo)
#	@$(call clean_pkg, KM_LLHPS)

# vim: syntax=make
