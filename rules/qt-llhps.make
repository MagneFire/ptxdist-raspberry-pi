# -*-makefile-*-
#
# Copyright (C) 2016 by Darrel Griet<dgriet@gmail.com>
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_QT_LLHPS) += qt-llhps

#
# Paths and names
#
QT_LLHPS_VERSION	:= 1.0
QT_LLHPS		:= qt-llhps-$(QT_LLHPS_VERSION)
QT_LLHPS_URL		:= file://$(PTXDIST_WORKSPACE)/local_src/qt-llhps
QT_LLHPS_DIR		:= $(BUILDDIR)/$(QT_LLHPS)
QT_LLHPS_BUILD_OOT	:= YES
QT_LLHPS_LICENSE	:= LGPL-2.1

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

#$(QT_LLHPS_SOURCE):
#	@$(call targetinfo)
#	@$(call get, QT_LLHPS)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

#QT_LLHPS_CONF_ENV	:= $(CROSS_ENV)

#
# qmake
#
#QT_LLHPS_CONF_TOOL	:= qmake
#QT_LLHPS_CONF_OPT	:= $(CROSS_QMAKE_OPT) PREFIX=/usr

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/qt-llhps.targetinstall:
	@$(call targetinfo)

	@$(call install_init, qt-llhps)
	@$(call install_fixup, qt-llhps, PRIORITY, optional)
	@$(call install_fixup, qt-llhps, SECTION, base)
	@$(call install_fixup, qt-llhps, AUTHOR, "Darrel Griet<dgriet@gmail.com>")
	@$(call install_fixup, qt-llhps, DESCRIPTION, missing)

#	#
#	# example code:; copy all binaries
#	#
	@$(call install_copy, qt-llhps, 0, 0, 0755, $(QT_LLHPS_DIR)-build/qt-llhps, /usr/bin/qt-llhps);
	# Install the startup script.
	@$(call install_copy, qt-llhps, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/etc/systemd/system/qt-llhps.service, /etc/systemd/system/qt-llhps.service)
	@$(call install_link, qt-llhps, ../qt-llhps.service, /etc/systemd/system/multi-user.target.wants/qt-llhps.service)


	#@for i in $(shell cd $(QT_LLHPS_PKGDIR) && find bin sbin usr/bin usr/sbin -type f); do \
	#	$(call install_copy, qt-llhps, 0, 0, 0755, -, /$$i); \
	#done

#	#
#	# FIXME: add all necessary things here
#	#

	@$(call install_finish, qt-llhps)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

#$(STATEDIR)/qt-llhps.clean:
#	@$(call targetinfo)
#	@$(call clean_pkg, QT_LLHPS)

# vim: syntax=make
